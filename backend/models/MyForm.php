<?php
namespace backend\models;

use yii\base\Model;

class MyForm extends Model
{
    public $name;
    public $invoice;
    public $summ;

    public function rules()
    {
        return [
            [['name', 'invoice'], 'string'],
            [['summ'], 'number'],
            [['name', 'invoice', 'summ'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'summ' => 'Баланс',
            'invoice' => '№ счёта',
        ];
    }

}