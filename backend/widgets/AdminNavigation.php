<?php
namespace backend\widgets;

use yii\base\Widget;

/**
 * Виджет меню админки
 *
 * Class AdminNavigation
 */
class AdminNavigation extends Widget
{
    /**
     * @var array
     */
    public $items = [
        [
            'label' => 'Главная',
            'url' => '/admin',
            'class' => 'fa fa-home',
            'routes' => ['admin/index']
        ],
        [
            'label' => 'Моя организация',
            'url' => '/admin/partner/my-organisation',
            'class' => '',
            'routes' => ['partner']
        ],
        [
            'label' => 'Пользователи',
            'url' => '/admin/user',
            'routes' => ['user']
        ],
        [
            'label' => 'Контрагенты',
            'url' => '/admin/partner',
            'class' => '',
            'routes' => ['partner']
        ],
        [
            'label' => 'Счета',
            'url' => '/admin/invoice',
            'class' => '',
            'routes' => ['invoice']
        ],
        [
            'label' => 'Операции',
            'url' => '/admin/operation',
            'class' => '',
            'routes' => ['operation']
        ],
        [
            'label' => 'Импотр файла',
            'url' => '/admin/operation/import',
            'class' => '',
            'routes' => ['operation']
        ],
    ];

    /**
     * Инициализация виджета
     *
     * @param array $items
     */
    public function init(array $items = [])
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('admin-navigation/main', [
            'items' => $this->items,
        ]);
    }
}