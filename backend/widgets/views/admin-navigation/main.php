<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="profile-element">
            <a href="/" target="_blank">
                <span> <strong class="font-bold">Администрирование</strong></span><br>
            </a>
        </div>
    </li>
    <?php
    $route = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
    ?>
    <?php foreach ($items as $item) : ?>
            <li class="<?= in_array($route, $item['routes']) ? 'active' : '' ?>">
                <a href="<?= $item['url'] ?? '' ?>"><i class="<?= $item['class'] ?? '' ?>">
                    </i> <span class="nav-label"><?= $item['label'] ?? '' ?></span>
                    <?php if(!empty($item['subItems'])) : ?>
                        <span class="fa arrow"></span>
                    <?php endif; ?>
                </a>
                <?php if(!empty($item['subItems'])) : ?>
                    <ul class="nav nav-second-level collapse">
                        <?php foreach ($item['subItems'] as $subItem) : ?>
                            <?php if($subItem['permission'] && Yii::$app->user->can($subItem['permission'])): ?>
                                <li class="<?= in_array($route, $subItem['routes']) ? 'active' : '' ?>"><a href="<?= $subItem['url'] ?? '' ?>"><?= $subItem['label'] ?? '' ?></a></li>
                            <?php endif;?>
                        <?php endforeach; ?>
                    </ul>
            </li>
        <?php endif;?>
    <?php endforeach; ?>
</ul>