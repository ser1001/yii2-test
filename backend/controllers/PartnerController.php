<?php
namespace backend\controllers;

use common\models\partner\Partner;
use common\models\invoce\Invoice;
use backend\models\MyForm;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
Use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class PartnerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'my-organisation'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new Partner();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columns =
            [
                ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
                [
                    'attribute'=>'name',
                    'vAlign'=>'middle',
                    'order'=>DynaGrid::ORDER_FIX_LEFT
                ],
                [
                    'attribute'=>'email',
                    'vAlign'=>'middle',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->user->email;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(User::find()->asArray()->all(), 'id', 'email'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'email'],
                    'format'=>'raw'
                ],
                [
                    'attribute'=>'invoice_number',
                    'vAlign'=>'middle',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->invoice->number;
                    },
                ],
                [
                    'attribute'=>'invoice_summ',
                    'vAlign'=>'middle',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->invoice->summ;
                    },
                ]
            ];

        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $columns,
        ]);
    }

    /**
     *
     */
    public function actionMyOrganisation()
    {
        $my_organisation = Partner::findOne(['is_me' => true]);
        $my_invoice = Invoice::findOne(['partner_id' => $my_organisation->id]);
        $model = new MyForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $partner = new Partner();
                $partner->name = $model->name;
                $partner->is_me = true;
                $partner->save();

                $invoice = new Invoice();
                $invoice->number = $model->invoice;
                $invoice->summ = $model->summ;
                $invoice->partner_id = $partner->id;
                $invoice->save();

                $user = User::findOne(['id' => Yii::$app->user->getId()]);
                $user->partner_id = $partner->id;
                $user->save();

                Yii::$app->session->setFlash('success', 'Сохранено');
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('my-organisation',[
            'model' => $model,
            'my_organisation' => $my_organisation,
            'my_invoice' => $my_invoice,
        ]);

    }
}