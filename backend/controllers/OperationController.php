<?php

namespace backend\controllers;

use backend\models\UploadForm;
use common\models\invoce\Invoice;
use common\models\partner\Partner;
use http\Exception;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use moonland\phpexcel\Excel;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use common\models\operation\Operation;
use yii\web\UploadedFile;

class OperationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'import'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new Operation();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columns =
            [
                ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
                [
                    'attribute'=>'date',
                    'filterType'=>GridView::FILTER_DATE,
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['format'=>'yyyy-mm-dd']
                    ],
                ],
                'invoice_number',
                [
                    'attribute'=>'partner_id',
                    'vAlign'=>'middle',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->partner->name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(Partner::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'РљРѕРЅС‚СЂР°РіРµРЅС‚'],
                    'format'=>'raw'
                ],
                'summ',

                [
                    'class'=>'kartik\grid\BooleanColumn',
                    'attribute'=>'is_inc_direction',
                    'vAlign'=>'middle',
                ],

            ];

        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $columns,
        ]);
    }

    public function actionUpdate($id = null)
    {
        if (isset($id)){
            $model = Operation::findOne($id);
        }
        else{
            $model = new Operation();
        }
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            try {
                $model->save(Yii::$app->request->post());
                Yii::$app->session->setFlash('success', 'Успешно');
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update',[
            'model' => $model
        ]);
    }

    public function actionImport()
    {
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                $fileName = $model->file->tempName;
                $data = Excel::import($fileName, [
                    'setFirstRecordAsKeys' => false,
                    'setIndexSheetByName' => true,
                    'getOnlySheet' => 'sheet1',
                ]);
                foreach (array_slice($data, 1) as $operation){
                    $date = date('Y-m-d', strtotime($operation['B']));
                    $partner = Partner::findOne(['name' => $operation['D']]);
                    $invoice = Invoice::findOne(['number' => $operation['C']]);
                    if ($operation['F'] == 'Входящая') {
                        $operation['F'] = true;
                    }
                    elseif ($operation['F'] == 'Исходящая') {
                        $operation['F'] = false;
                    }

                    if (empty($partner)){
                        $newPartner = new Partner();
                        $newPartner->name = $operation['D'];
                        if ($newPartner->validate()){
                            $newPartner->save();
                        }
                    }
                    if (empty($invoice)){
                        $newInvoice = new Invoice();
                        $newInvoice->number = $operation['C'];
                        $newInvoice->summ = $operation['E'];
                        if (!empty($newPartner->id)){
                            $newInvoice->partner_id = $newPartner->id;
                        }
                        else{
                            $newInvoice->partner_id = $partner->id;
                        }

                        if ($newInvoice->validate()){
                            $newInvoice->save();
                        }
                    }
                    $operationFind = Operation::find()
                        ->andWhere(['date' => $date])
                        ->andWhere(['invoice_number' => $operation['C']])
                        ->andWhere(['partner_id' => $partner->id])
                        ->andWhere(['summ' => $operation['E']])
                        ->andWhere(['is_inc_direction' => $operation['F']])
                        ->one();
                    if (empty($operationFind)){
                        $newOperation = new Operation();
                        $newOperation->date = $date;
                        $newOperation->invoice_number = $operation['C'];
                        $newOperation->partner_id = $partner->id;
                        if (!empty($newPartner->id)){
                            $newOperation->partner_id = $newPartner->id;
                        }
                        else{
                            $newOperation->partner_id = $partner->id;
                        }
                        $newOperation->summ = $operation['E'];
                        $newOperation->is_inc_direction = $operation['F'];
                        if ($newOperation->validate()){
                            $newOperation->save();
                            Yii::$app->session->setFlash('success', 'Успешно');
                        }
                        $my_organisation = Partner::findOne(['is_me' => true]);
                        $my_invoice = Invoice::findOne(['partner_id' => $my_organisation->id]);
                        $partner_invoice = Invoice::findOne(['number' => $operation['C']]);
                        if ($operation['F'] == true){
                            $my_invoice->summ = $my_invoice->summ + $operation['E'];
                            $partner_invoice->summ = $partner_invoice->summ - $operation['E'];
                            $my_invoice->save();
                            $partner_invoice->save();
                        }
                        elseif ($operation['F'] == false){
                            $my_invoice->summ = $my_invoice->summ - $operation['E'];
                            $partner_invoice->summ = $partner_invoice->summ + $operation['E'];
                            $my_invoice->save();
                            $partner_invoice->save();
                        }
                    }
                }
            }
        }

        return $this->render('import', [
            'model' => $model
        ]);
    }
}
