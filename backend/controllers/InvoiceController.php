<?php
namespace backend\controllers;

use common\models\invoce\Invoice;
use common\models\partner\Partner;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class InvoiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new Invoice();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columns =
            [
                ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
                [
                    'attribute'=>'number',
                    'pageSummary'=>'Итого:',
                    'vAlign'=>'middle',
                    'order'=>DynaGrid::ORDER_FIX_LEFT
                ],

                [
                    'attribute'=>'summ',
                    'hAlign'=>'right',
                    'vAlign'=>'middle',
                    'format'=>['decimal', 2],
                    'pageSummary'=>true
                ],
                [
                    'attribute'=>'partner_id',
                    'vAlign'=>'middle',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->partner->name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(Partner::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Контрагент'],
                    'format'=>'raw'
                ],
            ];

        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $columns,
        ]);
    }

}
