<?php

namespace backend\controllers;

use common\models\partner\Partner;
use common\models\User;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class UserController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new User();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columns =
            [
                ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
                'username',
                'email',
                [
                'attribute'=>'partner_id',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model->partner->name;
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>ArrayHelper::map(Partner::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'Контрагент'],
                'format'=>'raw'
            ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                                return Html::a('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>', ['user/update?id=' . $model->id], [
                                    'class' => 'btn btn-sm btn-primary'
                                ]);
                        },
                    ],
                ]
            ];

        return $this->render('index', [
            'columns' => $columns,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = User::findOne($id);
        $partners = ArrayHelper::map(Partner::find()->orderBy('name')->asArray()->all(), 'id', 'name');

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', 'Успешно');
        }

        return $this->render('update',[
            'model' => $model,
            'partners' => $partners,
        ]);
    }

}
