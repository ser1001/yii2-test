<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/* @var $model common\models\operation\Operation */
/* @var $form yii\widgets\ActiveForm */
/* @var $columns array */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel common\models\operation\Operation */
?>

<div class="row">
    <div class="col-md-4">
        <?= Html::a('Добавить <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>', ['update'], ['class'=>'btn btn-primary']) ?>
        <?= Html::a('Импорт <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>', ['import'], ['class'=>'btn btn-warning']) ?>
    </div>
</div>
<br>

<div class="row">
    <?php
    DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-info',
        'showPersonalize'=>true,
        'storage' => 'session',
        'gridOptions'=>[
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'responsiveWrap'=>false,
        ],
        'options'=>['id'=>'dynagrid-1']
    ]);
    DynaGrid::end();
    ?>
</div>
