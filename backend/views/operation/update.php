<?php

/* @var $this yii\web\View */
/* @var $model common\models\operation\Operation */

$this->title = 'Изменение операции ' ;
$this->params['breadcrumbs'][] = ['label' => 'Операции', 'url' => ['index']];
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>