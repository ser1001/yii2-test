<?php

use yii\widgets\ActiveForm;

/* @var $model \backend\models\UploadForm */
?>

<?php $form = ActiveForm::begin() ?>
    <h2>Импорт операций</h2>
<?= $form->field($model, 'file')->fileInput()->label(false) ?>

<button>Загрузить</button>

<?php ActiveForm::end() ?>