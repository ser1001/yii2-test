<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MyForm */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if (empty($my_organisation)) : ?>

    <div class="form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'invoice')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'summ')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php else : ?>

    <div class="form-group">
        <label for="inputPassword">Название организации:</label>
        <input type="text" class="form-control" value="<?= $my_organisation->name ?>" readonly="readonly">
    </div>

    <div class="form-group">
        <label for="inputPassword">Номер счёта:</label>
        <input type="text" class="form-control" value="<?= $my_invoice->number ?>" readonly="readonly">
    </div>

    <div class="form-group">
        <label for="inputPassword">Текущий баланс:</label>
        <input type="text" class="form-control" value="<?= $my_invoice->summ ?>" readonly="readonly">
    </div>

<?php endif; ?>


