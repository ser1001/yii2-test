<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\widgets\AdminNavigation;

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <head>
        <title><?= $this->title?></title>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?= AdminNavigation::widget() ?>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <?= Html::a('<i class="fa fa-sign-out"></i> Выйти', ['/site/logout'], [
                                'data' => [
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </li>
                    </ul>
                </nav>
            </div>
            <!--    breadcrumbs    -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?= $this->title?></h2>
                    <?= Breadcrumbs::widget([
                        'homeLink' => ['label' => 'Главная', 'url' => '/admin'],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <?= Alert::widget() ?>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer">
                <p class="pull-left">&copy; <?= date('Y') ?></p>
            </div>

        </div>
        <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>