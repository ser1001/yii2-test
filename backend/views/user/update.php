<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>

<h2>Редактирование пользователя</h2>
<h5>Привяжите пользователя и контрагента</h5>
<div class="form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        <?= $form->field($model, 'partner_id')->dropDownList($partners) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
