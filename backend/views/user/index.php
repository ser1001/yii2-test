<?php

use kartik\dynagrid\DynaGrid;

DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'panel-info',
    'showPersonalize'=>true,
    'storage' => 'session',
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>true,
        'pjax'=>true,
        'responsiveWrap'=>false,
    ],
    'options'=>['id'=>'dynagrid-1']
]);
DynaGrid::end();
?>
