<?php
use kartik\dynagrid\DynaGrid;

/* @var $this yii\web\View */
/* @var $model common\models\invoce\Invoice */
/* @var $form yii\widgets\ActiveForm */
/* @var $columns array */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel common\models\invoce\Invoice */
?>

<?php
DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'panel-info',
    'showPersonalize'=>true,
    'storage' => 'session',
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>true,
        'pjax'=>true,
        'responsiveWrap'=>false,
    ],
    'options'=>['id'=>'dynagrid-1']
]);
DynaGrid::end();
?>
