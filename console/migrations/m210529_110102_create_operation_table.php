<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%operation}}`.
 */
class m210529_110102_create_operation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%operation}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->notNull(),
            'invoice_number' => $this->integer(255)->notNull(),
            'partner_id' => $this->integer(255)->notNull(),
            'summ' => $this->float()->notNull(),
            'is_inc_direction' => $this->boolean()->notNull(),
        ]);
        $this->addForeignKey(
            'invoice_number',
            'operation',
            'invoice_number',
            'invoice',
            'number'
        );
        $this->addForeignKey(
            'partner_id',
            'operation',
            'partner_id',
            'partner',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%operation}}');
    }
}
