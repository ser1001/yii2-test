<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%partner}}`.
 */
class m210531_131317_add_is_me_column_to_partner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner}}', 'is_me', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner}}', 'is_me');
    }
}
