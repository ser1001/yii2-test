<?php

use yii\db\Migration;

/**
 * Class m210528_125528_add_admin_to_user
 */
class m210528_125528_add_admin_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = (new \yii\db\Query())
            ->select('id')
            ->from('user')
            ->where(['username' => 'admin']);

        $adminId =  $query->scalar();

        $userRole = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->assign($userRole, $adminId);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
