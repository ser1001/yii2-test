<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invoice}}`.
 */
class m210529_064328_create_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%invoice}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(255)->notNull()->unique(),
            'summ' => $this->float()->notNull(),
            'partner_id' => $this->integer(),
        ]);
        $this->addForeignKey(
            'partner_id',
            'invoice',
            'partner_id',
            'partner',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%invoice}}');
    }
}