<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%violation}}`.
 */
class m210531_133847_create_violation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%violation}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'date' => $this->dateTime(),
            'category' => $this->string(),
        ]);
        $this->addForeignKey(
            'user_id',
            'violation',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%violation}}');
    }
}
