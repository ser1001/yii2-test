<?php

use yii\db\Migration;

/**
 * Class m210528_124545_create_user_admin
 */
class m210528_124545_create_user_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('user');
        $this->batchInsert('user', [
            'username',
            'auth_key',
            'password_hash',
            'email',
            'status',
            'created_at',
            'updated_at'
        ], [
            [
                'admin',
                Yii::$app->security->generateRandomString(),
                Yii::$app->security->generatePasswordHash('123456'),
                'admin@site.com',
                10,
                time(),
                time()
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['username' => 'admin']);
    }
}
