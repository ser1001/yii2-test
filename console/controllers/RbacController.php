<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;


class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $admin = $auth->createRole('admin');
        $partner = $auth->createRole('partner');

        $auth->add($admin);
        $auth->add($partner);

        $viewAdminPage = $auth->createPermission('viewAdminPage');
        $viewAdminPage->description = 'Просмотр админки';

        $viewFrontend = $auth->createPermission('viewFrontend');
        $viewFrontend->description = 'Просмотр сайта';

        $auth->add($viewAdminPage);
        $auth->add($viewFrontend);

        $auth->addChild($partner,$viewFrontend);

        $auth->addChild($admin, $partner);

        $auth->addChild($admin, $viewAdminPage);

    }
}