<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-md-4">
        <?= Html::a('Создать <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>', ['/operation-add'], ['class'=>'btn btn-primary']) ?>
    </div>
</div>
<br>

<div class="row">
    <?php
    DynaGrid::begin([
        'columns' => $columns,
        'theme'=>'panel-info',
        'showPersonalize'=>true,
        'storage' => 'session',
        'gridOptions'=>[
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'responsiveWrap'=>false,
        ],
        'options'=>['id'=>'dynagrid-1']
    ]);
    DynaGrid::end();
    ?>
</div>
