<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Восстановление пароля';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block2">
    <div class="site-login">
        <div class="row">
            <div class="col-xs-3">
            </div>
            <div class="col-lg-5">
                <h2 align="center">Восстановление пароля</h2>
                <p align="center">Введите адрес электронной почты</p>
                <h1><?= Html::encode($this->title) ?></h1>

                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
