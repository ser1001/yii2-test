<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Войти';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block2">
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-xs-3">
        </div>
        <div class="col-lg-5">
            <h2 align="center">Авторизация</h2>
            <p align="center">Заполните все поля для авторизации:</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Email') ?>

                <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>

                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомнить') ?>

                <div style="color:#999;margin:1em 0">
                    Если вы забыли пароль, нажмите <?= Html::a('восстановить', ['site/request-password-reset']) ?>.
                    <br>
                    Нет учётной записи? <?= Html::a('Зарегистрироваться', ['/register']) ?>
                </div>

                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
        </div>
</div>
</div>


