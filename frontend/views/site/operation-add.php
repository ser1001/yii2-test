<?php

use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>

<div class="form">

    <?php $form = ActiveForm::begin(); ?>

    <label class="control-label">Получатель:</label>
    <input type="text" class="form-control" value="<?= $partner->name ?>" placeholder="Введите email" readonly>
    <label class="control-label">Номер счёта:</label>
    <input type="email" class="form-control" value="<?= $invoice->number ?>" placeholder="Введите email" readonly>

    <?= $form->field($model, 'summ')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>