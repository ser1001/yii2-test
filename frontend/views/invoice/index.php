<?php
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

?>

<?= Html::a('Добавить <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>', ['#'], ['class'=>'btn btn-primary']) ?>

<?= DynaGrid::widget([
    'gridOptions' => [
        'dataProvider' => $dataProvider,
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'number',
        'summ',
    ],
    'options' => ['id' => 'dynagrid-partner'],
]); ?>
