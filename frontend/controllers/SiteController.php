<?php
namespace frontend\controllers;

use common\models\invoce\Invoice;
use common\models\operation\Operation;
use common\models\partner\Partner;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup','logout'],
                        'allow' => true,
//                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'operation', 'operation-add'],
                        'allow' => true,
                        'roles' => ['partner'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest){

            $this->redirect('/login');
        }

        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('Учётная запись создана', 'Спасибо за регистрацию');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {

                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    /**
     * @return mixed
     */
    public function actionOperation()
    {
        if (Yii::$app->user->isGuest){

            $this->redirect('/login');
        }
        $searchModel = new Operation();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columns =
            [
                ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
                [
                    'attribute'=>'date',
                    'filterType'=>GridView::FILTER_DATE,
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['format'=>'yyyy-mm-dd']
                    ],
                ],
                [
                    'attribute'=>'invoice_number',
                    'vAlign'=>'middle',
                    'label' => 'Получатель',
                    'content'=> function($model){
                        $partner = Partner::findOne(['is_me' => true]);

                        return $partner->name;
                    }
                ],
                [
                    'attribute'=>'invoice_number',
                    'vAlign'=>'middle',
                    'label' => 'Счёт получателя',
                    'content'=> function($model){
                        $partner = Partner::findOne(['is_me' => true]);
                        $invoice = Invoice::findOne(['partner_id' => $partner->id]);

                        return $invoice->number;
                    }
                ],
                'summ',
                [
                    'attribute'=>'invoice_number',
                    'vAlign'=>'middle',
                    'label' => 'Тип операции',
                    'content'=>function($model){
                        if ($model->is_inc_direction == true){
                            $type = 'Исходящая';
                        }
                        else{
                            $type = 'Входящая';
                        }

                        return $type;
                    }
                ],
                [
                    'attribute'=>'invoice_number',
                    'vAlign'=>'middle',
                    'label' => 'Остаток',
                    'content'=>function($model){
                        $ostatok = [];
                        $my_summ = Invoice::findOne(['partner_id' => Yii::$app->user->identity->partner_id]);
                        $summ = $my_summ->summ;
                        $operation = Operation::find()->where(['partner_id' => Yii::$app->user->identity->partner_id])->orderBy(['id' => SORT_DESC])->all();
                        foreach ($operation as $value){
                            if ($value->is_inc_direction == true) {
                                $summ = $summ + $value->summ;
                                $summ2 = $summ - $value->summ;
                                $ostatok[$value->id] = $summ2;
                            }
                            if ($value->is_inc_direction == false) {
                                $summ = $summ - $value->summ;
                                $summ2 = $summ + $value->summ;
                                $ostatok[$value->id] = $summ2;
                            }
                        }

                        return $ostatok[$model->id];
                    }
                ],
            ];

        return $this->render('operation',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $columns,
        ]);
    }
    /**
     * @return mixed
     */
    public function actionOperationAdd()
    {
        if (Yii::$app->user->isGuest){

            $this->redirect('/login');
        }
        $model = new Operation();
        $partner = Partner::findOne(['is_me' => true]);
        $invoice = Invoice::findOne(['partner_id' => $partner->id]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

        }

        return $this->render('operation-add',[
            'model' => $model,
            'partner' => $partner,
            'invoice' => $invoice,
        ]);
    }
}
