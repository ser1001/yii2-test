<?php

namespace frontend\controllers;

use common\models\invoce\Invoice;
use common\models\partner\Partner;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class InvoiceController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['partner'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $partner = Partner::findOne(['email' => Yii::$app->user->identity->email]);
        $query = Invoice::find()->where(['partner_id' => $partner->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }

}
