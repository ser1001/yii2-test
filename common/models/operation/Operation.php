<?php

namespace common\models\operation;

use backend\models\object\Object_typeModel;
use common\models\invoce\Invoice;
use common\models\partner\Partner;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "operation".
 *
 * @property int $id
 * @property string|null $date
 * @property int|null $invoice_number
 * @property int|null $partner_id
 * @property float|null $summ
 * @property bool|null $is_inc_direction
 *
 * @property Invoice $invoiceNumber
 * @property Partner $partner
 */
class Operation extends \yii\db\ActiveRecord
{
    public $invoice_one;
    public $invoice_two;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['invoice_number', 'partner_id'], 'default', 'value' => null],
            [['invoice_number', 'partner_id'], 'integer'],
            [['summ'], 'number'],
            [['invoice_one', 'invoice_two'], 'number'],
            [['is_inc_direction'], 'boolean'],
            [['invoice_number'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::class, 'targetAttribute' => ['invoice_number' => 'number']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'invoice_number' => 'Номер счёта',
            'partner_id' => 'Контрагент',
            'summ' => 'Сумма',
            'is_inc_direction' => 'Входящая операция',
            'invoice_one' => 'Отправитель',
            'invoice_two' => 'Получатель',
        ];
    }

    /**
     * Gets query for [[InvoiceNumber]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceNumber()
    {
        return $this->hasOne(Invoice::class, ['id' => 'invoice_number']);
    }

    /**
     * Gets query for [[Partner]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::class, ['id' => 'partner_id']);
    }

    /**
     * Список счетов.
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->joinWith('partner');

        if (!Yii::$app->user->can('admin')){
            $query->andWhere(['partner_id' => Yii::$app->user->getId()]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'date' => $this->date,
            'partner.id' => $this->invoice_one,
            'partner.id' => $this->invoice_two,
            'summ' => $this->summ,
            'is_inc_direction' => $this->is_inc_direction

        ]);

        return $dataProvider;
    }

    /**
     * Название контрагента.
     *
     * @return string
     */
    public function getPartnerName(){
        $query = Partner::findOne(['id' => $this->partner_id]);
        $result = $query->name;

        return $result;
    }

    /**
     * Список контрагентов
     *
     * @return array
     */
    public function getPartnerList(){

        return ArrayHelper::map(Partner::find()
            ->asArray()
            ->all(),'id', 'name');
    }

    /**
     * Список счетов
     *
     * @return array
     */
    public function getInvoiceList(){

        return ArrayHelper::map(Invoice::find()
            ->asArray()
            ->all(),'number', 'number');
    }
}
