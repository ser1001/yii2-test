<?php

namespace common\models\invoce;

use common\models\partner\Partner;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property int $number
 * @property float $summ
 * @property int|null $partner_id
 *
 * @property Partner $partner
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'summ','partner_id'], 'required'],
//            [['number', 'partner_id'], 'default', 'value' => null],
//            [['id','number', 'partner_id'], 'integer'],
//            [['summ'], 'number'],
//            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер счёта',
            'summ' => 'Сумма',
            'partner_id' => 'Контрагент',
        ];
    }

    /**
     * Gets query for [[Partner]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * Список счетов.
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'number' => $this->number,
            'partner_id' => $this->partner_id,
            'summ' => $this->summ
        ]);

        return $dataProvider;
    }

    /**
     * Название контрагента.
     *
     * @return string
     */
    public function getPartnerName(){
        $query = Partner::findOne(['id' => $this->partner_id]);
        if (!empty($query->name)){
            $result = $query->name;
        }
        else{
            $result = 'Наш счёт';
        }

        return $result;
    }

    /**
     * Баланс контрагента
     *
     * @return string
     */
    public static function getUserInvoice(){
        $query = Invoice::findOne(['partner_id' => Yii::$app->user->identity->partner_id]);

        return $query;
    }
}
