<?php

namespace common\models\partner;

use common\models\invoce\Invoice;
use common\models\User;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 *
 * @property Invoice[] $invoices
 */
class Partner extends \yii\db\ActiveRecord
{
    public $invoice_number;
    public $invoice_summ;
    public $email;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['email'], 'string'],
            [['invoice_number'], 'integer'],
            [['invoice_summ'], 'number'],
            [['is_me'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'email' => 'Email',
            'invoice_number' => '№ счёта',
            'invoice_summ' => 'Баланс',
        ];
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::class, ['partner_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['partner_id' => 'id']);
    }

    /**
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partner::find()
            ->joinWith('invoice')
            ->joinWith('user');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['invoice.number' => $this->invoice_number])
            ->andFilterWhere(['invoice.summ' => $this->invoice_summ]);

        return $dataProvider;
    }
}
